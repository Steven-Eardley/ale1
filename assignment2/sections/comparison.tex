\section{System Comparison}

\subsection{AutoTutor Emotions}
From the outset AutoTutor was designed to improve students' learning by simulating a human tutor~\cite{AT_human}, providing interactive dialogue to help college-level learners give in-depth answers to questions about computer literacy, or physics. It assists a student to learn via a conversation (in which the computer dictates and shows text and the student types responses) using an animated 'talking head' with gestures and facial expressions, which 'grounds' the conversation - provides a context for the dialogue to keep the student's attention. Some versions also include a simulation to further assist students. The concept AutoTutor is based on is called \emph{expectation and misconception tailored dialogue} (EMT dialogue), which is common in human tutors~\cite{emt}. The idea is to teach students by helping them reach the tutor's expected ideal answer, while correcting any misconceptions the student may show along the way.

Evaluations of AutoTutor have shown it has a positive impact on students' learning gains~\cite{dialogue_reading} where users show an improved capability for 'deep knowledge': an understanding of material including interconnectivity and detail.

AutoTutor Emotions (AT-E) builds on the base system by adding methods to detect and react to the perceived affective state (i.e. mood, emotional state) of the user, as this is widely thought to be connected to learning~\cite{frust_bored}. This required extra hardware for the system's use, as shown in Figure~\ref{gubbins} below.

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.80\textwidth]{figures/Emotions.jpg}
  \caption{Diagram of extra sensors used in AT-E - from the project's webpage~\cite{artsite}}
  \label{gubbins}
\end{figure}

Along with the extra hardware, AT-E used a standard classifier (logistic regression) to decide upon a state using the sensor data. The goal of this affect-sensitive extension was to push ITS research beyond purely cognitive bounds and introducing some idea of emotional recognition and reaction to the electronic tutoring process~\cite{towardaffect}.

AutoTutor Emotions was not deployed in any scale to be of benefit of users - the evaluation consisted of a small number of college students using the system for half an hour. The goal of initial evaluations was to determine if the system could correctly identify their affective states, rather than evaluate the teaching produced by AutoTutor. The students were learning about the computer literacy domain, but in a lab setting (and likely not intended to be used elsewhere due to the specific hardware). Later, this affect-sensitive version was compared to the base version, where affect-sensitivity was determined to be beneficial for low capability students, but hindered those with good grasp of the content~\cite{AT_affect}.

\subsection{Crystal Island - Outbreak}

Crystal Island - Outbreak is aimed at a younger audience: US middle school (10-14yr) students rather than college level (17+yr). The system centers around a full 3D game in which the user attempts to complete a story, requiring them to solve puzzles which need knowledge of microbiology and the scientific method~\cite{crystalsite}. The aim here is more engaging learning - a clear context and rich characters are designed to motivate the student to interact and explore the subject area~\cite{differences}. Learning is guided by inquiry and investigation.

Interaction with Crystal Island, while in part sharing philosophies with AutoTutor of learning through conversation with agents, differs by a lack of natural language input - the dialogue moves are accomplished by selecting items from a menu, a method standard in many computer games.

Figure~\ref{crystal} shows this interaction in practice. The user hears and reads the character's dialogue, and selects responses and actions from the menu on the right hand side of the screen. Apart from characters, the student can interact with the world by moving and inspecting items represented in the world, such as books or posters like that  visible in the background of Figure~\ref{crystal}, and taking notes within the game~\cite{SabourinExploring}.

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.80\textwidth]{figures/cio.png}
  \caption{Interaction with Crystal Island (from lecture slides~\cite{lecture})}
  \label{crystal}
\end{figure}

Crystal Island's use is intended and has been evaluated in a classroom setting through sessions of 45mins - 1 hour, a class period. The experiments showed that Crystal Island improved learners' problem solving skills and learning outcomes. However, those students who were already engaged with the subject were found to perform better in-game~\cite{rowe2011integrating}.

\subsection{Comparison Summary}

AutoTutor Emotions and Crystal Island - Outbreak have different strengths - while AT-E was found to give the best results for low ability students, Crystal Island was most successful when used by those who tested well before using the system. The overall approaches also differ: in Crystal Island the emphasis is on student incentive and initiative - an open-ended exploration - whereas AT-E gave a more rigid framework, i.e. tutor guidance to reach the required answer~\cite{AT_dialogue}. The target learner groups also differ, as mentioned above, with AutoTutor being focused on an older audience. It hasn't been deployed at such scale, however, and mostly exists as a research tool rather than a learning aid.

In terms of design, AutoTutor benefits from a modular approach, which has helped its adaptation in over a decade of research. The affect-sensitive variant is joined by projects with animations, different domains, variable feedback etc. In comparison Crystal Island's content is very much embedded in the game narrative - the whole scenario has been programmed to provide lessons within the virtual island world. As a platform, it is practically much less adaptable.