\section{Design Recommendations}

This section is a recommendation for an Adaptive Learning Environment for teaching driving skills based on AutoTutor Emotions, for reasons specified below. While initially it may seem intuitive for the system to be based on the 3D Crystal Island - Outbreak due to its real-time interaction, it would be more beneficial to add these capabilities to AutoTutor and make use of its sophisticated affect-sensitive framework.

\subsection{Task}

For this scenario our driving skills tutor is to be given the task of simulating a driving lesson with an instructor in a simulated semi-interactive city environment. The emphasis will be on safety and awareness, outlined by two key points:

\begin{enumerate}
    \item Procedure for checks when driving, e.g. "mirrors, signal, manoeuvre"
    \item Identification of hazards while in motion (\`{a} la DVLA hazard perception test)
\end{enumerate}

The scope does not include: physical vehicle operation (gears, clutch etc), complete highway code information, or an open-world simulation with unbounded actions. Therefore, its use will be focused on mid to late-stage learner drivers of any (driving) age who can operate a vehicle but would benefit from an introduction to common mistakes during driving tests.

\subsection{Approach}

The ALE would use AutoTutor's natural language conversation to help guide learners to provide all of the relevant information before the simulation progresses. For example, driving students are often asked for cognitive walkthrough or to 'think-aloud' as they perform tasks to show the instructor they meet the key requirements assessed. The tasks necessary for safe driving are documented and broken down in handbooks and guides~\cite{drivingsite} and could be assessed in part using AutoTutor Emotions' existing systems - using student's language to build the knowledge model each time the task is undertaken, or using eye tracking to check the subject is looking in the correct place, e.g. mirrors in a simulated cockpit.

Item 2 in particular will require augmentation of AutoTutor Emotions with a 3D simulator component. The simulator is not only an intuitive platform for demonstrating real-world techniques, their use improves engagement with learning~\cite{sims}. The simulator's task can be simplified by limiting interaction - for example, only allowing control over the vehicle throttle and brake. This would remove cognitive load of learning some system operation specifics: not all users will have experience playing driving games so may be put off by full freedom of movement control, which in turn may distract from the learning goals. Advanced users may benefit from a completely immersive scenario, so this could be an adaptation permitted according to user skill, however by using just the throttle and allowing the system to automatically steer, the environment can be scripted. This would give gold-standard comparisons for the user, while allowing assessment of response time, e.g. emergency stop during hazard tests. Full control would be worth investigating, but vastly expands the scope and complexity, so this would likely be unfeasible.

User adaptation could focus on providing support when the user seems agitated or panicked, as detected by the posture and eye sensors. The tutor may suggest a break, pause the simulation and recap information, or tell witty jokes.

The system would be assessed by improvements to users performance on DVLA hazard perception tests. Computer games are thought to improve reaction times and pattern recognition of players~\cite{game-drivingsite}. Since the test checks your response time, a quantitative evaluation could be undertaken.

\subsection{Interaction Example}

The learning environment would look much like that in Figure~\ref{gubbins} - a set of monitors giving a wrap-around cockpit simulation, with the extra technology tracking the user's state. As the user is asked to set off in the car, the tutor will prime the user to discuss their steps, aiming for a complete demonstration of the safety checks. The simulated car will move at the student's command along a city street. In this phase the hazard perception will be tested, where eye tracking ensures the user can identify and respond to simulated hazards in the world. To further make use of dialogue, the AutoTutor instructor may ask for information relevant to the simulation, e.g. who has right of way at junctions as the user reaches them. When mistakes are made, the tutor could immediately pause the simulation and demonstrate the hazard. Bonus points if AutoTutor's 'talking head' agent shows fear.

\subsection{Discussion}

This system is really the best of both worlds between Crystal Island and AT-E. Recommending AutoTutor as the basis is mainly due to the presence of goal tracking via narrative, which is similar to how driving instructors teach. The re-purposing of hardware from detecting state to being part of the learning goals would likely be a relatively easy task, with much prior work in the eye-tracking field~\cite{Cruz-neira93surround-screenprojection-based}. The addition of an interactive simulation would require a good deal of effort, but there is a wealth of experience from the games industry to draw upon, and the result can be easily incorporated in AutoTutor's modular design.

The teaching methods also are better aligned with driving - in a 'mission critical' safety domain an investigative and exploratory approach runs the risk of leaving the student unclear of the rigid regime required to pass a driving test, whereas the guided approach from AutoTutor gives a more explicit lesson.